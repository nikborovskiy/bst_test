<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use app\models\Currency;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
$date = date('Y-m-d');
?>
<div class="site-index">

    <h1>Курс валют</h1>
    <div class="row">
        <div class="col-md-4">
            <?= Html::dropDownList('valueID', null, Currency::getCurrencyList(), ['class' => 'form-control']); ?>
        </div>
        <div class="col-md-4">
            <?= DateRangePicker::widget([
                'name' => 'date',
                'value' => $date . '_' . $date,
                'language' => Yii::$app->language,
                'convertFormat' => true,
                'pluginOptions' => [
                    'timePicker' => true,
                    'timePickerIncrement' => 30,
                    'locale' => [
                        'format' => 'Y-m-d',
                        'separator' => '_'
                    ]
                ]
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= Html::button('Показать данные', ['class' => 'btn btn-success js-apply-btn']) ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="currency-table" class="table table-bordered">
                <tr>
                    <th>#</th>
                    <th>Идентификатор валюты</th>
                    <th>Числовой код валюты</th>
                    <th>Буквенный код валюты</th>
                    <th>Имя валюты</th>
                    <th>Значение курса</th>
                    <th>Дата курса</th>
                </tr>
                <tr class="js-row-data">
                    <td colspan="7">
                        Нет данных для отображения
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
