<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property int $id #
 * @property string|null $valuteID Идентификатор валюты
 * @property string|null $numCode Числовой код валюты
 * @property string|null $сharCode Буквенный код валюты
 * @property string|null $name Имя валюты
 * @property float|null $value Значение курса
 * @property string|null $date Дата публикации курса
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'integer'],
            [['date'], 'safe'],
            [['valuteID', 'numCode', 'сharCode', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'valuteID' => 'Идентификатор валюты',
            'numCode' => 'Числовой код валюты',
            'сharCode' => 'Буквенный код валюты',
            'name' => 'Имя валюты',
            'value' => 'Значение курса',
            'date' => 'Дата публикации курса',
        ];
    }

    /**
     * Получение данных за период для конкретного идентификатора валюты
     * @param $valueID //идентификатор валюты
     * @param $date // период (если период не указан, то беруться данные за текущий день)
     * @return array
     * @throws NotFoundHttpException
     */
    public static function getCurrencyData($valueID, $date)
    {
        $result = [];
        if ($date === null) {
            $currentDay = date('Y-m-d');
            $date = $currentDay . '_' . $currentDay;
        }
        $range = explode('_', $date);
        if (count($range) != 2) {
            throw new NotFoundHttpException();
        }
        $from = $range[0];
        $to = $range[1];
        /** @var Currency[] $models */
        $models = Currency::find()
            ->andWhere(['valuteID' => $valueID])
            ->andWhere(['>=', 'date', $from])
            ->andWhere(['<=', 'date', $to])
            ->orderBy('date DESC')
            ->all();

        if ($models) {
            foreach ($models as $model) {
                $result[] = $model->getAttributes();
            }
        }
        return $result;
    }

    /**
     * Список всех валют
     * @return array
     */
    public static function getCurrencyList()
    {
        $models = Currency::find()->select(['valuteID', 'name'])->groupBy(['valuteID', 'name'])->all();
        return ArrayHelper::map($models, 'valuteID', 'name');
    }

    /**
     * Преобразование значения валюты в формат с плавающей точкой и 4 знаками после запятой
     */
    public function afterFind()
    {
        parent::afterFind();
        if ($this->value) {
            $this->value = $this->value/10000;
            $values = explode('.', $this->value);
            $this->value = $values[0].','.str_pad($values[1], 4, '0000');
        }
    }
}
