//получение и вывод данных
function getAndSetData() {
    let valueId = $('select[name="valueID"]').val();
    let date = $('[name="date"]').val();
    $.get('/site/get-currency-data', {valueID: valueId, date: date}, function (data) {
        if (data && data.length) {
            let $table = $('#currency-table');
            $table.find('.js-row-data').remove();
            let rows = '';
            $.each(data, function (index, value) {
                rows += '<tr class="js-row-data">';
                $.each(value, function (index, val) {
                    rows += '<td>' + val + '</td>';
                });
                rows += '</tr>';
            });
            $table.find('tr:last').after(rows);
        }
        return true;
    }, 'json');
}

// получение ошибок импорта и их вывод
function getImportError() {
    $.get('/site/get-import-error-data', function (data) {
        if (data && 'status' in data && data.status == 1) {
            if ($('#main_alert').length) {
                $.pjax.reload({container: '#main_alert', async: false});
            }
        }
        return true;
    }, 'json');
}

$(document).ready(function () {
    getAndSetData();
    //каждые 10 секунд проверяем на ошибки импорта
    setInterval(function () {
        getImportError();
    }, 10000);
});

$('.js-apply-btn').on('click', function (e) {
    getAndSetData();
});