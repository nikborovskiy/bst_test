<?php

namespace app\components;

class ErrorFilesHelper
{
    const REFRESH_CURRENCY_ERROR_FILE_NAME = 'refresh_currency_error.txt';

    /**
     * Получение данных файла
     * @param $fileName
     * @return false|string|null
     */
    public static function getFileData($fileName)
    {
        $result = null;
        $filePath = \Yii::$app->getRuntimePath() . DIRECTORY_SEPARATOR . $fileName;
        if (file_exists($filePath)) {
            $result = file_get_contents($filePath);
            unlink($filePath);
        }
        return $result;
    }

}