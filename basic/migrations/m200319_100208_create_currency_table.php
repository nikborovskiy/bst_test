<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%currency}}`.
 */
class m200319_100208_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey()->comment('#'),
            'valuteID' => $this->string()->comment('Идентификатор валюты'),
            'numCode' => $this->string()->comment('Числовой код валюты'),
            'сharCode' => $this->string()->comment('Буквенный код валюты'),
            'name' => $this->string()->comment('Имя валюты'),
            'value' => $this->integer()->comment('Значение курса'),
            'date' => $this->date()->comment('Дата публикации курса'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
