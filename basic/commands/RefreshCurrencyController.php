<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Currency;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\httpclient\Client;
use yii\httpclient\Response;

class RefreshCurrencyController extends Controller
{
    const URL_XML_DAILY = 'http://www.cbr.ru/scripts/XML_daily.asp';
    const URL_XML_DYNAMIC = 'http://www.cbr.ru/scripts/XML_dynamic.asp';
    const ERROR_FILE_NAME = 'refresh_currency_error.txt';

    /**
     * @return int
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $result = 0;
        $this->deleteErrorFile();
        $to = date('d/m/Y');
        $from = date('d/m/Y', strtotime('-30 days'));
        $dailyFile = $this->getRequestData(self::URL_XML_DAILY, ['date_req' => $to]);
        $sql = '';
        $tableName = Currency::getTableSchema()->name;
        if ($dailyFile && $dailyFile->Valute) {
            foreach ($dailyFile->Valute as $valute) {
                $idCurrency = (string)$valute->attributes()->ID;
                $numCode = $valute->NumCode;
                $charCode = $valute->CharCode;
                $name = $valute->Name;
                // не участвуют
//                $value = $valute->Value;
//                $nominal = $valute->Nominal;
                $dynamicFile = $this->getRequestData(self::URL_XML_DYNAMIC, ['date_req1' => $from, 'date_req2' => $to, 'VAL_NM_RQ' => $idCurrency]);
                if ($dynamicFile && $dynamicFile->Record) {
                    foreach ($dynamicFile->Record as $record) {
                        $val = str_replace(',', '.', $record->Value) * 10000;
                        $sql .= 'INSERT INTO `' . $tableName . '`(`valuteID`, `numCode`, `сharCode`, `name`, `value`, `date`)
                         SELECT "' . $idCurrency . '","' . $numCode . '","' . $charCode . '","' . $name . '",' . $val . ',"' . date('Y-m-d', strtotime((string)$record->attributes()->Date)) . '" 
                         WHERE NOT EXISTS (SELECT id FROM `' . $tableName . '` WHERE valuteID="' . $idCurrency . '" AND date = "' . date('Y-m-d', strtotime((string)$record->attributes()->Date)) . '");';
                    }
                }

            }
        }
        if ($sql) {
            /** @var \yii\db\Connection $db */
            $result = \Yii::$app->getDb()->createCommand($sql)->execute();
        } else {
            $this->addError('При импорте данных произошла ошибка.');
        }
        return $result;
    }

    /**
     * Получение данных по указанному url и параметрам
     * @param string $url
     * @param array $params
     * @return bool|\SimpleXMLElement
     * @throws \yii\base\InvalidConfigException
     */
    public function getRequestData($url, $params)
    {
        $result = false;
        try {
            $client = new Client();
            /** @var Response $response */
            $response = $client->createRequest()
                ->setMethod('get')
                ->setUrl($url)
                ->setData($params)
                ->send();
            if ($response->isOk) {
                $result = simplexml_load_string($response->getContent());
            } else {
                $this->addError('При импорте произошла ошибка. Попробуйте чуть позже.');
            }
        } catch (\Exception $exception) {
            $this->addError($exception->getMessage());
        }

        return $result;
    }

    /**
     * Путь к файлу с ошибкой
     * @return string
     */
    public function getErrorFilePath()
    {
        return \Yii::$app->getRuntimePath() . DIRECTORY_SEPARATOR . self::ERROR_FILE_NAME;
    }

    /**
     * Удаление файла с ошибками
     * @return bool
     */
    public function deleteErrorFile()
    {
        $filePath = $this->getErrorFilePath();
        if (file_exists($filePath)) {
            return unlink($filePath);
        }
        return true;
    }

    /**
     * Запись ошибки в файл
     * @param $text
     * @return false|int
     */
    public function addError($text)
    {
        return file_put_contents($this->getErrorFilePath(), $text . "\r\n", FILE_APPEND);
    }
}